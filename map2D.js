 var countIslands = 0;
 var inputString ="...0..0.\n..00.0.\n.000.\n0....0";
function map2D(stringRequest)
{
   var arrayLines = stringRequest.split("\n");
   
   for (i = 0; i < arrayLines.length; i++)
   {
       for(j=0; j < arrayLines[i].length; j++)
       {
           if(arrayLines[i].charAt(j) == "0")
           {
               if(i == 0)
               {
                    if (!adjacentLeft(i,j,arrayLines))
                        countIslands ++;
                    else if (!adjacentRight(i,j,arrayLines,arrayLines[i].length))
                        countIslands ++;
               }
               else
               {
                   if (!adjacentLeft(i,j,arrayLines))
                        if (!adjacentRight(i,j,arrayLines,arrayLines[i].length))
                            if (!adjacentUp(i,j,arrayLines)) 
                                if(!adjacentDown(i,j,arrayLines,arrayLines.length))
                                    countIslands ++;
               }
           }
       }
   }
}
function adjacentLeft(i,j,arrayLines)
{
    var response = false;
    if(j > 0)
    {
        if(arrayLines[i].charAt(j-1) == "0")
            response = true
        else
            response = false
    }
    return response;
}
function adjacentRight(i,j,arrayLines,length)
{
    var response = false;
    if(j < length)
    {
        if(arrayLines[i].charAt(j + 1) == "0")
            response = true
        else
            response = false
    }
    return response;
}
function adjacentUp(i,j,arrayLines)
{
    var response = false;
    try
    {
        if(i > 0)
        {
            if(arrayLines[i-1].charAt(j) == "0")
                response = true
            else
                response = false
        }
    }
    catch
    {
        response = false;
    }
    return response;
}
function adjacentDown(i,j,arrayLines, length)
{
    try
    {
        var response = false;
        if(i < length)
        {
            if(arrayLines[i+1].charAt(j) == "0")
                response = true
            else
                response = false
        }
    }
    catch
    {
        response = false;
    }
    return response;
}

map2D(inputString);
console.log("La cantidad de islas es: " + countIslands);
console.log(inputString);
