var A  = new Set([1,2]);
var B = new Set([1,2,3]);

function isSubsetOf(A, B)
{
    var response = true;
    for (var elem of A) {
        if (! B.has(elem))
            response = false;
    }
    return response;
}

function isSupersetOf(A, B)
{
    var response = true
    for (var elem of B) {
        if (! A.has(elem))
            response = false;
    }
    return response;
}
console.log(isSubsetOf(A , B));
console.log(isSubsetOf(B , A));
console.log(isSupersetOf(A , B));
console.log(isSupersetOf(B , A));
